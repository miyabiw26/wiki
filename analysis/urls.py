from django.urls import path
from . import views

app_name = "analysis"

urlpatterns = [
    path('', views.SearchView.as_view(),name="index"),
    path('result/', views.DisplayView.as_view(),name="result"),
]
