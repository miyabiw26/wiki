from django.shortcuts import render
from django.views import generic

import requests
import pandas as pd
import numpy as np
from pandas import Series,DataFrame
import datetime

class SearchView(generic.TemplateView):
    template_name = "analysis/form.html"


class DisplayView(generic.TemplateView):
    template_name = "analysis/result.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["search1"] = self.request.GET.get("search1")
        context["search2"] = self.request.GET.get("search2")
        context["search3"] = self.request.GET.get("search3")
        context["start"] = self.request.GET.get("start")
        context["end"] = self.request.GET.get("end")

        key1 = self.request.GET.get("search1")
        key2 = self.request.GET.get("search2")
        key3 = self.request.GET.get("search3")

        start_year = self.request.GET.get("start_year")
        start_month = self.request.GET.get("start_month")
        start_day = self.request.GET.get("start_day")
        end_year = self.request.GET.get("end_year")
        end_month = self.request.GET.get("end_month")
        end_day = self.request.GET.get("end_day")

        if len(start_month) == 1:
            start_month = "0"+start_month
        if len(start_day) == 1:
            start_day = "0"+start_day
        if len(end_month) == 1:
            end_month = "0"+end_month
        if len(end_day) == 1:
            end_day = "0"+end_day


        start = start_year+start_month+start_day
        end = end_year+end_month+end_day

        view_lsit_lsit = []
        for key in [key1,key2,key3]:
            url = f"https://wikimedia.org/api/rest_v1/metrics/pageviews/per-article/en.wikipedia/all-access/user/{key}/daily/{start}/{end}"
            r = requests.get(url)
            print(url)

            date_list = []
            view_list = []
            for k in range(len(r.json()["items"])):
                date_list.append(r.json()["items"][k]["timestamp"])
                view_list.append(r.json()["items"][k]["views"])
            view_lsit_lsit.append(view_list)

        # context["search_result_date"] = [datetime.datetime.strptime(s,'%Y%m%d%H') for s in date_list]
        context["search_result_date"] = [int(s) for s in date_list]
        context["search_result_view"] = view_lsit_lsit

        return context
